import {createConnection} from "typeorm"
import {Client} from "./entities/Client"
import {Banker} from "./entities/Banker"
import { Transaction} from "./entities/Transaction"
import express from "express"
import { createClientRouter } from "./routes/create_client"
import { createBankerRouter } from "./routes/create_banker"
import {createTransactionRouter} from "./routes/create_transaction"
import { connectBankerToClientRouter } from "./routes/connect_banker_to_client"
import { deleteClientRouter } from "./routes/delete_client"
import {fetchClientsRouter} from "./routes/fetch_clients"


const app= express()
app.use(express.json())
app.use(createClientRouter)
app.use(createBankerRouter)
app.use(createTransactionRouter)
app.use(connectBankerToClientRouter)
app.use (deleteClientRouter)
app.use (fetchClientsRouter)

const main = async () =>{
   try{
    const connection = await createConnection({
        type:"postgres",
        host:"localhost",
        port:5432,
        username:'postgres',
        password:'myPassword',
        database:'typeorm',
        entities:[Client,Banker,Transaction],
        synchronize:true 
         })
    console.log('connected to postgres')

    app.listen(8080,()=>{
      console.log("App is running on port 8080")
    })
   }catch(error){
     console.log('Unable to connect postgres')
     throw new Error("Unable to connect to db")
     
   }
}
main()