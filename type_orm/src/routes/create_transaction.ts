import expree from "express"
import { Client } from "../entities/Client";
import { Transaction ,TarnsactionsTypes} from "../entities/Transaction"

const router = expree.Router();

router.post('/api/client/:clientId/transaction',async(req,res)=>{
    const {clientId} = req.params;
    const {type,amount}= req.body;
    const client = await Client.findOne(parseInt(clientId));
    if(!client){
        return res.json({
            msg: "Client not found"
        })
    }
    const transacation = Transaction.create({
        amount,
        type,
        client
    });
    await transacation.save();
    if (type === TarnsactionsTypes.DEPOSIT){
        client.balance=client.balance+amount
    }
    else if(type===TarnsactionsTypes.WITHDRAW){
        client.balance=client.balance-amount
    }

    await client.save()
    return res.json({
        msg: "Transactions added"
    })

});

export {
    router as createTransactionRouter
}