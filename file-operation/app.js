const express = require("express")
const multer = require("multer")
const path = require('path')
const fs = require('fs')
const { I18n } = require('i18n')

const i18n = new I18n({
  locales: ['en', 'de'],
  directory: path.join(__dirname, 'locales')
})



const app = express()
app.get("/welcome", (req, res) => {
  i18n.init(req, res)
  res.end(res.__('Hallo'))
})

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/")
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname)
       },
       
})

const uploadStorage = multer({ 
    storage: storage,
    limits: {
    fileSize: 1000000
    },
fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(txt)$/)) {
        return cb(new Error('Please upload an text file'))
    }

    cb(undefined, true)
} });

// Single file
app.post("/upload", uploadStorage.single("file"), (req, res) => {
  console.log(req.file)
  return res.send("Single file")
})

app.listen(3000 || process.env.PORT, () => {
    console.log("Server on port 3000")
  })

  console.log("Going to write into existing file");
fs.writeFile('./uploads/file', 'new addition', function(err) {
   if (err) {
      return console.error(err);
   }
   
   console.log("Data written successfully!");
   console.log("Let's read newly written data");
   
   fs.readFile('./uploads/file', function (err, data) {
      if (err) {
         return console.error(err);
      }
      console.log("Asynchronous read: " + data.toString());
   });
});
console.log("Let's append file");
fs.appendFile('./uploads/file', 'data to append', function (err) {
    if (err) throw err;
    console.log('Saved!');
  });

