Websocket API

-Websocket make possible two way interactive communication session between client and server.
-using websocket you can send message to server and receive event-driven reposes without having to poll the server for reply

interfaces:

websocket:the primary interface for connecting to websocket server 
and sending and receiving data on the connection

closeevent:the event for close connection

messageevent: the event for receiving message from server

Examples:
AsyncAPI,HumbleNet,clusterWS,CWS,Socket.IO,SocketCluster

