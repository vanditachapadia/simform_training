Configure FTP Server in Ubantu OS
Steps::

1.First update the os 
  sudo apt-get update

2.get install FTP Server 
   sudo apt-get install vsftpd

3.create configure file
   sudo gedit /etc/vsftpd.conf

4.add user for ftp usage
  sudo adduser ftpuser

5.go to home directory of ftpuser
  cd /home/ftpuser/

6.create files in ftpuser home directory
  sudo touch file1 file2 file3 file4

7.check ipaddress of system
  ifconfig
  192.168.0.106

8.open second terminal window to create client side floders and file(if use only one system for server and client)
  mkdir client
  cd client/
  touch f1 f2 f3 f4 f5

9.to connect ftp server go to client terminal and perform below command and give username and password
  ftp 192.168.0.106

10.use get command for retrive one file
   get file1<filename>

   mget file2 file3 file4 
   for multiple files retrival

11.restart ftp server (server terminal)
    sudo service vsftpd restart

12.upload file to server from client side
   put f1<filename>
   mput f1 f2 f3 f4 f5
   to upload multiple file

13.terminate the session
   bye


reference:https://www.youtube.com/watch?v=YWhEjLu2_p4 







