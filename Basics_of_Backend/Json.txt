What is JSON?

JSON stands for JavaScript Object Notation
JSON is a lightweight data-interchange format
JSON is plain text written in JavaScript object notation
JSON is used to send data between computers
JSON is language independent *

JSON is Like XML Because

Both JSON and XML are "self describing" (human readable)
Both JSON and XML are hierarchical (values within values)
Both JSON and XML can be parsed and used by lots of programming languages
Both JSON and XML can be fetched with an XMLHttpRequest

JSON is Unlike XML Because

JSON doesn't use end tag
JSON is shorter
JSON is quicker to read and write
JSON can use arrays
The biggest difference is:

XML has to be parsed with an XML parser. JSON can be parsed by a standard JavaScript function.

-Why JSON is Better Than XML

XML is much more difficult to parse than JSON.
JSON is parsed into a ready-to-use JavaScript object.

For AJAX applications, JSON is faster and easier than XML



JSON Values
In JSON, values must be one of the following data types:

a string
a number
an object
an array
a boolean
null

JSON values cannot be one of the following data types:

a function
a date
undefined

syntax:
person = {name:"John", age:31, city:"New York"};





