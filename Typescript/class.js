var car = /** @class */ (function () {
    function car(wheel) {
        this.wheel = wheel;
    }
    car.prototype.display = function () {
        console.log("Car Wheels are:" + this.wheel);
    };
    return car;
}());
var honda = new car(2);
honda.display();
//# sourceMappingURL=class.js.map