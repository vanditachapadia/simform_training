var Day;
(function (Day) {
    Day[Day["MORNING"] = 0] = "MORNING";
    Day[Day["AFTERNOON"] = 1] = "AFTERNOON";
    Day[Day["EVENING"] = 2] = "EVENING";
})(Day || (Day = {}));
;
var time = 10;
if (time >= 1 && time <= 12) {
    console.log(Day.MORNING);
}
else if (time >= 13 && time <= 17) {
    console.log(Day.AFTERNOON);
}
else {
    console.log(Day.EVENING);
}
