namespace StringUtility {

    export function ToCapital(str: string): string {
        return str.toUpperCase();
    }

    export function ToLower(str:string): string {
        return str.toLowerCase();
    }
}