let sum=function(a:number,b:number){
    return a+b;
}
console.log(sum(20,30));

//function constructor

var myFunction = new Function("a", "b", "return a * b"); 
var x = myFunction(4, 3); 
console.log(x);

// lambda function

var devide =(y:number)=>{
    y= y/2;
    console.log(y);
}
devide(12);