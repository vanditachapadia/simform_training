var StringUtility;
(function (StringUtility) {
    function ToCapital(str) {
        return str.toUpperCase();
    }
    StringUtility.ToCapital = ToCapital;
    function ToLower(str) {
        return str.toLowerCase();
    }
    StringUtility.ToLower = ToLower;
})(StringUtility || (StringUtility = {}));
