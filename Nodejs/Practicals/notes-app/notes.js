const fs =require('fs')
const chalk = require('chalk')

const addNote=(title,body)=>{
  
    const notes=loadNotes()
    const duplicateNotes=notes.filter((note)=> note.title===title)
    
    debugger

    if (duplicateNotes.length==0){
    notes.push({
        title:title,
        body:body
    })

    saveNotes(notes)
    console.log("New node added!! ")
}
else{
    console.log("Note title taken!")
}

}

const saveNotes=(notes)=>{
    const dataJSON= JSON.stringify(notes)
    fs.writeFileSync('notes.json',dataJSON)

}

const loadNotes= ()=>{
    try{
  const dataBuffer = fs.readFileSync('notes.json')
  const dataJson =  dataBuffer.toString()
  return JSON.parse(dataJson)
    }
    catch(e){
        return []
    }

}

const removeNote=(title)=>{
    const notes=loadNotes()
    const notesToKeep=notes.filter((note)=>note.title!==title)
    
    if (notes.length>notesToKeep.length){
        console.log(chalk.green.inverse("Note removed!"))
        saveNotes(notesToKeep)
    }
    else{
        console.log(chalk.red.inverse("NO Note Found!"))
    }
  
}

const listNotes=()=>
{
    const notes=loadNotes()
    console.log(chalk.inverse("Your Notes"))
    notes.forEach(note => {
        console.log(note.title);
    });

}

const readNote=(title)=>{
    const notes=loadNotes()
    const Notetitle = notes.find((note)=>note.title===title)
    if(Notetitle){
        console.log(chalk.inverse("Read Note"))
        console.log(Notetitle.title)
        console.log(Notetitle.body)
    }
    else{
        console.log(chalk.red.inverse("Note Not Found"))
    }
}

module.exports = {
   
    addNote:addNote,
    removeNote:removeNote,
    listNotes:listNotes,
    readNote:readNote

}