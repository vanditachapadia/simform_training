
//object shorthand
const name="Vandita"
const  userAge=20

const user={
     name,
    userAge,
    location:'India'
}
//console.log(user)

// object destructring

const product={
    lable:'Red notebook',
    price:5,
    stock:201,
    saleprice:undefined
}

// const lable=product.lable
// const price=product.price

// const {lable:productLable,price,rating=5}=product
// console.log(productLable)
// console.log(price)
// console.log(rating)

const transaction=(type,{lable,stock})=>{
   console.log(type,lable,stock)
}
transaction('order',product)

