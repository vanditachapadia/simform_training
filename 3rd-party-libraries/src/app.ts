import "reflect-metadata";

import {plainToClass} from 'class-transformer';
import _ from 'lodash';
import { Product } from './product.model';
import { validate } from "class-validator";

const products =[{title:'A Carpet',price:29.99},
{title:'A Book' ,price:10.34}
];

const newProd = new Product('',-5.99);
validate(newProd).then(error=>{
 if(error.length>0){
  console.log('Validation errors!!');
  console.log(error);
 }else{
 console.log(newProd.getInformation());
 }
})




//const p1=new Product('A Book',12.99);

//console.log(p1.getInformation());
// declare var GLOBAL:any;

//console.log(_.shuffle([1, 2, 3]));

// console.log(GLOBAL)


// const  loadedProducts = products.map(prod=>{
//     return new Product(prod.title,prod.price);
// })

const loadedproducts = plainToClass(Product,products)

for(const prod of loadedproducts){
    console.log(prod.getInformation());
}