import { Injectable } from '@nestjs/common';
import { Customer } from 'src/customer/types/Customer';
import { CreateCustomerDto } from 'src/customer/dtos/CreateCustomer.dto';

@Injectable()
export class CustomersService {
  private customers: Customer[] = [
    {
      id: 1,
      email: 'prem@gmail.com',
      name: 'prem',
    },
    {
      id: 2,
      email: 'raj@gmail.com',
      name: 'raj',
    },
  ];

  findCustomerById(id: number) {
    return this.customers.find((user) => user.id === id);
  }

  createCustomer(customerDto: CreateCustomerDto) {
    this.customers.push(customerDto);
  }

  getCustomers() {
    return this.customers;
  }
}
