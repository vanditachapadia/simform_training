const express = require('express')
const app = express();
const port = 8081

app.get('/order-list', (req, res) => {
    let result = {
        data: {
            item: [
                {
                    id: 1,
                    name: 'order 1'
                },
                {
                    id: 2,
                    name: 'order 2'
                }
            ]
        }

    }
    res.status(200).json(result)
})

app.get('/', (req, res) => {
    res.send("Order Route")
})

app.listen(port, () => {
    console.log(`Order service is listening at http:localhost:${port}`)
})