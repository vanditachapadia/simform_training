const express = require('express');
const app = express();
const gateway = require('fast-gateway')
const port = 9001;
const checkauth = (req, res, next) => {
    console.log(req.header);
    next();
}

const server = gateway({
    routes: [
        {
            prefix: '/order',
            target: 'http://localhost:8081/',
            hooks: {

            }
        },
        {
            prefix: '/payment',
            middlewares: [checkauth],
            target: 'http://localhost:8082/',
            hooks: {

            }
        }
    ]
})

server.get('/mytesting', (req, res) => {
    res.send("gateway is called");
})
server.start(port).then(server => {
    console.log(`Api is running on port ${port}`);
})