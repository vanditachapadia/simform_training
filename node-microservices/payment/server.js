const express = require('express')
const app = express();
const port = 8082

app.get('/payment-list', (req, res) => {
    let result = {
        data: {
            item: [
                {
                    id: 1,
                    name: 'payment 1'
                },
                {
                    id: 2,
                    name: 'payment 2'
                }
            ]
        }

    }
    res.status(200).json(result)
})

app.get('/', (req,res) => {
    res.send("Payment Route")
})
app.listen(port, () => {
    console.log(`payment service is listening at http:localhost:${port}`)
})