module.exports = {
    openapi: "3.0.3", // present supported openapi version
    info: {
      title: "Simple Todos API", // short title.
      description: "A simple todos API", //  desc.
      version: "1.0.0", // version number
      contact: {
        name: "Vandita Chapadia", // your name
        email: "vandita@gmail.com", // your email
        url: "abc.com", // your website
      },
    },
  };