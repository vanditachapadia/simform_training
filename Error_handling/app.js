const express = require("express");
const multer = require("multer");
const ejs = require("ejs");
const path = require("path");

const storage = multer.diskStorage({
	destination: "./public/uploads",
	filename: function (req, file, cb) {
		cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
	},
});

const upload = multer({
	storage: storage,
	limits: {
		fileSize: 100000,
	},
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	},
}).single("myImg");

function checkFileType(file, cb) {
	const filetypes = /jpeg|jpg|gif|png/;

	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

	const mimetype = filetypes.test(file.mimetype);

	if (mimetype && extname) {
		return cb(null, true);
	} else {
		cb("Error: Images Only");
	}
}
const app = express();
app.use(express.json());
app.set("view engine", "ejs");

app.use(express.static("./public"));

app.get("/", (req, res) => res.render("index"));


app.post("/uploads", (req, res) => {
	upload(req, res, (err) => {
		if (err) {
			res.render("index", {
				msg: err,
			});
		} else {
			if (req.file == undefined) {
				res.render("index", {
					msg: "Error: No file",
				});
			} else {
				res.render("index", {
					msg: "file uploaded",
					file: `/uploads/${req.file.filename}`,
				});
			}
			console.log(req.file);
		}
	});
});


app.get("/user", (req, res) => {
	try {
		const user = req.body.name;
		if (user == "") {
			//custom error message
			throw new Error("Provide user name");
		} else {
			res.status(200).send("Welcome:" + user);
		}
	} catch (e) {
		res.status(400).send("Error:" + e);
	}
});
const port = 3000;
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});
