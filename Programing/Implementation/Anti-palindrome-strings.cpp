#include <bits/stdc++.h>
using namespace std;
int main(){
	int t;
	cin>>t;
	while(t--){
		string str;
		cin>>str;
	    string rev = string(str.rbegin(),str.rend());
		if(str==rev){
			cout<<-1<<endl;
		}
		else{
			sort(str.begin(), str.end());
			cout<<str<<endl;
		}


	}
}