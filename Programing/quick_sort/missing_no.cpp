#include<bits/stdc++.h>

using namespace std;

int main(){

    ios_base::sync_with_stdio(0);

    cin.tie(0);

    int t;cin>>t;

    while(t--){

        int n,x = 2;cin>>n;

        int A[n];

        for(int i = 0; i<n; i++)cin>>A[i];

        sort(A,A+n);

        for(int i = 0; i<n; i++){

            if(A[i]>=x)x+=2;

        }

        cout<<x<<endl;

    }

    return 0;

}