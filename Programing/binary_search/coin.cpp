#include<bits/stdc++.h>

using namespace std;

#define ll long long

vector<ll>v;

ll leftsum=0,rightsum=0;

bool possible=false;

void calculate(ll midNo)

{

auto it=find(v.begin(),v.end(),midNo);

if(it==v.end())

{

for(auto ite=v.begin();ite<=lower_bound(v.begin(),v.end(),midNo)-1;ite++)

{

leftsum+=*ite;

}

for(auto ite=lower_bound(v.begin(),v.end(),midNo);ite<v.end();ite++)

{

rightsum+=*ite;

}

}

else

{

int skip=*it;

for(auto ite=v.begin();ite<=lower_bound(v.begin(),v.end(),midNo)-1;ite++)

{

if(*ite!=skip)

leftsum+=*ite;

}

for(auto ite=lower_bound(v.begin(),v.end(),midNo);ite<v.end();ite++)

{

if(*ite!=skip)

rightsum+=*ite;

}

 

}

 

}

int main()

{

 

ios_base::sync_with_stdio(false);

cin.tie(NULL);

 

 

 

ll n;

cin>>n;

if(n==1)

{

cout<<"NO"<<endl;

return 0;

}

for(ll i=0;i<n;i++)

{

ll no;

cin>>no;

v.push_back(no);

}

sort(v.begin(),v.end());

ll high=*max_element(v.begin(),v.end())+1;

ll low=0,mid;

while(high-low>1)

{

mid=(high+low)/2;

calculate(mid);

if(leftsum==rightsum)

{

possible=true;

break;

}

if(leftsum<rightsum)

{

low=mid;

}

else

{

high=mid;

}

leftsum=0;

rightsum=0;

 

}

if(possible)

cout<<"YES"<<endl;

else

cout<<"NO"<<endl;

 

}