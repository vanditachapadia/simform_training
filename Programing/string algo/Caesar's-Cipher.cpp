#include<bits/stdc++.h>

using namespace std;

#define ll long long int

int main()

{

ll test_cases;

cin >> test_cases;

while(test_cases--)

{

string s, t;

cin >> s >> t;

int ans = -1;

bool flag = false;

for(int i = 0; i < (int)s.size(); i ++)

{

int diff = (t[i] - s[i] + 26) % 26;

if(ans == -1)

{

ans = diff;

}

if(diff == ans)

{

continue;

}

else

{

flag = true;

break;

}

}

if(flag == true)

{

cout<<"-1"<<endl;

}

else

{

cout<<ans<<endl;

}

}

return 0;

}