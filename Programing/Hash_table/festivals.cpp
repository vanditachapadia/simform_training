#include<bits/stdc++.h>

using namespace std;

typedef long long ll;

const ll mod=1e9+7;

int main()

{

ll t;

cin>>t;

while(t--)

{

ll n;

cin>>n;

map<string,vector<ll>>mp;

while(n--)

{

string s;

ll x;

cin>>s>>x;

mp[s].push_back(x);

}

map <string,vector<ll>>::iterator it1;

vector<ll>::iterator it2;

ll prev=INT_MIN;

string out;

for (it1=mp.begin();it1!=mp.end();it1++)

{

//cout <<(*it1).first<<" : ";

ll sum=0;

ll cnt=0;

// cout<<it1->first;

sort((*it1).second.begin(),(*it1).second.end(),greater<ll>());

for( it2 =(*it1).second.begin();it2 !=(*it1).second.end();it2++)

{

//cout <<*it2<< " ";

sum+=*it2;

cnt++;

if(cnt==3)

break;

}

if(sum>prev)

{

out=it1->first;

prev=sum;

}

//cout<<endl;

}

cout<<out<<" "<<prev<<endl;

 

}

