#include<bits/stdc++.h>

using namespace std;

typedef long long int ll;

typedef unsigned long long int ull;

typedef vector<int> vi;

typedef pair<int, int> pi;

#define FOR(a, c) for (int a = 0; a < c; a++)

#define FORL(a, b, c) for (int(a) = (b); (a) <= (c); (a)++)

#define FORR(a, b, c) for (int(a) = (b); (a) >= (c); (a)--)

#define max(a, b) (a < b ? b : a)

#define min(a, b) ((a > b) ? b : a)

#define F first

#define S second

#define PB push_back



 

int main(){

    ios_base::sync_with_stdio(0);

    cin.tie(0);

    int t, x1, x2, y1, y2;

    cin >> t;

    while(t--){

        cin >> x1 >> y1;

        cin >> x2 >> y2;

        int a = x1 - x2;

        int b = y1 - y2;

        if(abs(a) > 1 || abs(b) > 1){

            cout << "DRAW" << endl;

        }

        else if(x1 == x2 && y1 == y2){

            cout << "SECOND" << endl;

        }

        else{

            cout << "FIRST" << endl;

        }

    }

    return 0;

}