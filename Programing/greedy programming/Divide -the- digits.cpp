#include<bits/stdc++.h>
using namespace std;
int solve(string s){
	string first="",second="";
	 sort(s.begin(), s.end());
	int l=s.length();
	for(int i=0;i<l;i=i+2){
		if(l%2 && i==l-1){
			first+=s[i];
		}
		else{
			first+=s[i];
			second+=s[i+1];
		}

	}
	return stoi(first)+stoi(second);
}
int main(){
	int t;
	cin>>t;
	while(t--){
		long int n;
		cin>>n;
		string s=to_string(n);
		cout<<solve(s)<<endl;
	}
}